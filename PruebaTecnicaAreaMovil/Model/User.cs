﻿namespace PruebaTecnicaAreaMovil.Model
{
    public class User
    {
        public string Login { get; set; }
        public string AvatarUrl { get; set; }
        public string Name { get; set; }
    }
}
