﻿using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil.DB
{
    public interface IDataAccess
    {
        User GetUser();
        void SetUser(User user);
    }
}