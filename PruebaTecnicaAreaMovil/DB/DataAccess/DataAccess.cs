﻿using System;
using System.Linq;

using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil.DB
{
    public class DataAccess : IDataAccess
    {

        #region Propiedades

        IDatabase Database { get; set; }

        #endregion

        #region Ciclo de vida

        public DataAccess(IDatabase database)
        {
            Database = database;
        }

        #endregion

        #region DAO

        public User GetUser()
        {
            var user = Database.GetUsers().FirstOrDefault();
            return UserMapper.MapSingle(user);
        }

        public void SetUser(User user)
        {
            Database.DeleteAllUsers();
            Database.SaveUser(UserMapper.MapSingle(user));
        }

        #endregion

    }
}
