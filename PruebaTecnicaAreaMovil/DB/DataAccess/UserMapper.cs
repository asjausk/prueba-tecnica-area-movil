﻿using System.Collections.Generic;
using System.Linq;

using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil.DB
{
    static class UserMapper
    {
        public static User MapSingle(DBUser user)
        {
            if (user == null)
                return null;
            
            return new User
            {
                Login = user.Login,
                AvatarUrl = user.AvatarUrl,
                Name = user.Name
            };
        }

        public static DBUser MapSingle(User user)
        {
            if (user == null)
                return null;

            return new DBUser
            {
                Login = user.Login,
                AvatarUrl = user.AvatarUrl,
                Name = user.Name
            };
        }

        public static IEnumerable<User> Map(IEnumerable<DBUser> users)
        {
            return from user in users select MapSingle(user);
        }

        public static IEnumerable<DBUser> Map(IEnumerable<User> users)
        {
            return from user in users select MapSingle(user);
        }
    }
}
