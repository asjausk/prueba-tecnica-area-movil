﻿using Ninject.Modules;
using Realms;

namespace PruebaTecnicaAreaMovil.DB
{
    public class DatabaseModule : NinjectModule
    {
        RealmConfiguration RealmConfiguration()
        {
            return new RealmConfiguration("db.realm")
            {
                ShouldDeleteIfMigrationNeeded = true
            };
        }
        
        public override void Load()
        {
            Bind<RealmConfiguration>().ToConstant(RealmConfiguration());
            Bind<IDatabase>().To<Database>().InSingletonScope();
            Bind<IDataAccess>().To<DataAccess>().InSingletonScope();
        }
    }
}
