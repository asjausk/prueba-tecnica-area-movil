﻿using System.Collections.Generic;

namespace PruebaTecnicaAreaMovil.DB
{
    public interface IDatabase
    {
        IEnumerable<DBUser> GetUsers();
        void SaveUser(DBUser dBUser);
        void DeleteAllUsers();
    }
}
