﻿using Realms;

namespace PruebaTecnicaAreaMovil.DB
{
    public class DBUser : RealmObject
    {
        [PrimaryKey]
        public string Login { get; set; }
        public string AvatarUrl { get; set; }
        public string Name { get; set; }
    }
}
