﻿using System;
using System.Collections.Generic;

using Realms;

namespace PruebaTecnicaAreaMovil.DB
{
    class Database : IDatabase
    {

        Realm Realm => Realm.GetInstance(IoC.IoC.Current.Get<RealmConfiguration>());
        
        #region Implementación IDatabase

        public IEnumerable<DBUser> GetUsers()
        {
            var r = Realm;
            return r.All<DBUser>();
        }

        public void SaveUser(DBUser user)
        {
            var r = Realm;
            r.Write(() =>
            {
                r.Add(user, true);
            });
        }

        public void DeleteAllUsers()
        {
            var r = Realm;
            r.Write(() =>
            {
                r.RemoveAll<DBUser>();
            });
        }

        #endregion

    }
}
