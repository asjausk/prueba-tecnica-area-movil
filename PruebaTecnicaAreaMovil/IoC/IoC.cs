﻿using System;
using System.Linq;
using Ninject;
using Ninject.Modules;
using Ninject.Parameters;
using PruebaTecnicaAreaMovil.Api;
using PruebaTecnicaAreaMovil.DB;

namespace PruebaTecnicaAreaMovil.IoC
{
    public class IoC
    {
        public static IoC Current { get; set; }

        IKernel Kernel { get; set; }

        public IoC(INinjectModule[] modules)
        {
            Kernel = new StandardKernel(
                new INinjectModule[]
                {
                    new DatabaseModule(),
                    new ApiModule()
                }
                .Concat(modules)
                .ToArray()
            );
            Current = this;
        }

        public T Get<T>(params IParameter[] parameters)
        {
            return Kernel.Get<T>(parameters);
        }

        public T Get<T>(string name, params IParameter[] parameters)
        {
            return Kernel.Get<T>(name, parameters);
        }
    }
}
