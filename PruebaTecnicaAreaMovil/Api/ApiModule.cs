﻿using Ninject.Modules;
using Realms;

namespace PruebaTecnicaAreaMovil.Api
{
    public class ApiModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IClient>().To<Client>().InSingletonScope();
        }
    }
}
