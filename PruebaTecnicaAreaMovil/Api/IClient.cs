﻿using System.Threading.Tasks;
using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil.Api
{
    public interface IClient
    {
        Task<User> GetUser(string login);
    }
}