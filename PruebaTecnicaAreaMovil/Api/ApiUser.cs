﻿using Newtonsoft.Json;

namespace PruebaTecnicaAreaMovil
{
    class ApiUser
    {
        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
