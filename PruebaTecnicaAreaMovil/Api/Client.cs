﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil.Api
{
    public class Client : IClient
    {
        HttpClient Http { get; set; }

        public Client()
        {
            Http = new HttpClient
            {
                BaseAddress = new Uri("https://api.github.com/users/")
            };
            Http.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "queso");
        }

        public async Task<User> GetUser(string login)
        {
            var response = await Http.GetAsync(login);

            if (!response.IsSuccessStatusCode)
                return null;

            var body = await response.Content.ReadAsStringAsync();
            var user = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiUser>(body);

            return UserMapper.MapSingle(user);
        }
    }
}
