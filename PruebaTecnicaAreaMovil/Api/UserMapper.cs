﻿using System.Collections.Generic;
using System.Linq;

using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil
{
    static class UserMapper
    {
        public static User MapSingle(ApiUser user)
        {
            if (user == null)
                return null;

            return new User
            {
                Login = user.Login,
                AvatarUrl = user.AvatarUrl,
                Name = user.Name
            };
        }

        public static ApiUser MapSingle(User user)
        {
            if (user == null)
                return null;

            return new ApiUser
            {
                Login = user.Login,
                AvatarUrl = user.AvatarUrl,
                Name = user.Name
            };
        }

        public static IEnumerable<User> Map(IEnumerable<ApiUser> users)
        {
            return from user in users select MapSingle(user);
        }

        public static IEnumerable<ApiUser> Map(IEnumerable<User> users)
        {
            return from user in users select MapSingle(user);
        }
    }
}
