﻿using System.Collections.Generic;
using System.Linq;

using PruebaTecnicaAreaMovil.Model;

namespace PruebaTecnicaAreaMovil.iOS
{
    static class UserProfileMapper
    {
        public static User MapSingle(UserProfile user)
        {
            if (user == null)
                return null;

            return new User
            {
                Login = user.Login,
                AvatarUrl = user.AvatarUrl,
                Name = user.Name
            };
        }

        public static UserProfile MapSingle(User user)
        {
            if (user == null)
                return null;

            return new UserProfile
            {
                Login = user.Login,
                AvatarUrl = user.AvatarUrl,
                Name = user.Name
            };
        }

        public static IEnumerable<User> Map(IEnumerable<UserProfile> users)
        {
            return from user in users select MapSingle(user);
        }

        public static IEnumerable<UserProfile> Map(IEnumerable<User> users)
        {
            return from user in users select MapSingle(user);
        }
    }
}
