﻿using Ninject.Modules;

namespace PruebaTecnicaAreaMovil.iOS
{
    public class UserProfileModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserProfileViewModel>().To<UserProfileViewModel>();
            Bind<UserProfileViewController>().To<UserProfileViewController>();
        }
    }
}
