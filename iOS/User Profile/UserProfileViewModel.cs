﻿using System.Threading.Tasks;
using PruebaTecnicaAreaMovil.Api;
using PruebaTecnicaAreaMovil.DB;

namespace PruebaTecnicaAreaMovil.iOS
{
    public class UserProfileViewModel : IUserProfileViewModel
    {
        public Task<UserProfile> LoadCache()
        {
            return new TaskFactory().StartNew(() =>
            {
                var dao = IoC.IoC.Current.Get<IDataAccess>();
                var user = dao.GetUser();
                return UserProfileMapper.MapSingle(user);
            });
        }

        public async Task<UserProfile> SearchUser(string searchText)
        {
            var client = IoC.IoC.Current.Get<IClient>();
            var dao = IoC.IoC.Current.Get<IDataAccess>();

            var user = await client.GetUser(searchText);
            if (user != null)
                dao.SetUser(user);

            return UserProfileMapper.MapSingle(user);
        }
    }
}