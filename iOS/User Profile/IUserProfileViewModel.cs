﻿using System.Threading.Tasks;

namespace PruebaTecnicaAreaMovil.iOS
{
    public interface IUserProfileViewModel
    {
        Task<UserProfile> LoadCache();
        Task<UserProfile> SearchUser(string searchText);
    }
}
