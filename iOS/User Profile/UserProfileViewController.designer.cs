// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace PruebaTecnicaAreaMovil.iOS
{
    [Register ("UserProfileViewController")]
    partial class UserProfileViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint closedDataConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loginLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel nameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint notFoundHeightConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint openedDataConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISearchBar searchBar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (closedDataConstraint != null) {
                closedDataConstraint.Dispose ();
                closedDataConstraint = null;
            }

            if (imageView != null) {
                imageView.Dispose ();
                imageView = null;
            }

            if (loginLabel != null) {
                loginLabel.Dispose ();
                loginLabel = null;
            }

            if (nameLabel != null) {
                nameLabel.Dispose ();
                nameLabel = null;
            }

            if (notFoundHeightConstraint != null) {
                notFoundHeightConstraint.Dispose ();
                notFoundHeightConstraint = null;
            }

            if (openedDataConstraint != null) {
                openedDataConstraint.Dispose ();
                openedDataConstraint = null;
            }

            if (searchBar != null) {
                searchBar.Dispose ();
                searchBar = null;
            }
        }
    }
}