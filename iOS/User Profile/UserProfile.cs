﻿namespace PruebaTecnicaAreaMovil.iOS
{
    public class UserProfile
    {
        public string Login { get; set; }
        public string AvatarUrl { get; set; }
        public string Name { get; set; }
    }
}
