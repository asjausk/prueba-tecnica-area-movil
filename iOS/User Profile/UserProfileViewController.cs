﻿using System;
using System.Threading.Tasks;
using Foundation;
using UIKit;

namespace PruebaTecnicaAreaMovil.iOS
{
    public partial class UserProfileViewController : UIViewController, IUISearchBarDelegate
    {

        #region Propiedades

        IUserProfileViewModel ViewModel {
            get;
            set;
        }

        #endregion

        #region Constructor

        public UserProfileViewController(IUserProfileViewModel viewModel) : base("UserProfileViewController", null)
        {
            ViewModel = viewModel;
        }

        #endregion

        #region Ciclo de vida

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            searchBar.Delegate = this;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            Invoke(async () =>
            {
                await LoadCache();
            }, 0);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        #endregion

        #region Acciones

        async Task LoadCache()
        {
            var user = await ViewModel.LoadCache();
            DisplayUser(user);
        }

        async Task SearchUser(string searchText)
        {
            var user = await ViewModel.SearchUser(searchText);
            SetNotFoundActive(user == null);
            DisplayUser(user);
        }

        void DisplayUser(UserProfile user)
        {
            imageView.Image = null;
            if (user == null)
            {
                openedDataConstraint.Active = false;
                closedDataConstraint.Active = true;
            }
            else
            {
                nameLabel.Text = user.Name;
                loginLabel.Text = "@" + user.Login;
                openedDataConstraint.Active = true;
                closedDataConstraint.Active = false;

                LoadImageFromUrl(user.AvatarUrl);
            }
            UIView.Animate(0.25, () =>
            {
                View.LayoutIfNeeded();
            });
        }

        void SetNotFoundActive(bool val)
        {
            notFoundHeightConstraint.Constant = val ? 128 : 0;
            UIView.Animate(0.25, () =>
            {
                View.LayoutIfNeeded();
            });
        }

        Task LoadImageFromUrl(string urlString)
        {
            return new TaskFactory().StartNew(() =>
            {
                var url = NSUrl.FromString(urlString);
                var data = NSData.FromUrl(url);
                if (data == null)
                    return;
                var image = UIImage.LoadFromData(data);
                InvokeOnMainThread(() =>
                {
                    imageView.Image = image;
                });
            });
        }

        #endregion

        #region IUISearchBarDelegate

        [Export("searchBar:textDidChange:")]
        public void TextChanged(UISearchBar searchBar, string searchText)
        {
            if (searchText.Length == 0)
            {
                searchBar.SetShowsCancelButton(false, true);
            }
            else
            {
                searchBar.SetShowsCancelButton(true, true);
            }
        }

        [Export("searchBarCancelButtonClicked:")]
        public void CancelButtonClicked(UISearchBar searchBar)
        {
            searchBar.EndEditing(true);
            searchBar.Text = "";
            searchBar.SetShowsCancelButton(false, true);
        }

        [Export("searchBarSearchButtonClicked:")]
        public void SearchButtonClicked(UISearchBar searchBar)
        {
            searchBar.EndEditing(true);
            var searchText = searchBar.Text;
            if (searchText.Length == 0)
            {
                return;
            }

            Invoke(async () =>
            {
                await SearchUser(searchText);
            }, 0);
        }

        #endregion

    }
}
